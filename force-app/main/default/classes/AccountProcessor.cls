public class AccountProcessor{
    
    @future
    public static void countContacts(Set<Id> accids){
    Map<Id,List<Contact>> mapids = new Map<Id,List<Contact>>();
        List<Contact> liscon = [select id,accountId from Contact where accountId in :accids];
        List<Account> lisacc = [select id from Account where id in :accids];
        for(Contact con: liscon){
            if(!mapids.containsKey(con.accountId)){
                mapids.put(con.accountId, new List<Contact>{con});
            }else{
                mapids.get(con.accountId).add(con);
            }
        }
    
        
            for(Account accountt : lisacc){
            List<Contact> conn = mapids.get(accountt.id);
            accountt.Number_of_Contacts__c = conn.size();
            }
            system.debug('-----'+ mapids.values().size());
        
        
    }
    
}